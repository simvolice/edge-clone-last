#/bin/bash

TYPE=$1
VERSION=$2
if [ -z "$VERSION" ]; then
	VERSION="1.0.0"
fi
if [ "$TYPE" = "prod" ] || [ "$TYPE" = "pre-prod" ] || [ "$TYPE" = "test" ]; then
    BASEDIR=`pwd`
    cd "$BASEDIR"
    echo "Building $VERSION-$TYPE image"
    docker build -t registry.gitlab.com/bein.tech/workflow-edge:$VERSION-$TYPE .
    echo "Pushing $VERSION-$TYPE image to repository"
    docker push registry.gitlab.com/bein.tech/workflow-edge:$VERSION-$TYPE
else
    echo "TYPE argument is missing: prod, pre-prod, test. Ex: ./docker-build.sh test 1.0.0"
fi