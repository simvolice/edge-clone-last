// A wrapper module around the minio client that provides Promise-based API


// Reusable minio client
const Minio = require('minio')
const minioClient = new Minio.Client({
	endPoint: process.env.MINIO_HOST,
	port: parseInt(process.env.MINIO_PORT),
	useSSL: (process.env.MINIO_USE_SSL === 'true'),
	accessKey: process.env.MINIO_ACCESS_KEY,
	secretKey: process.env.MINIO_SECRET_KEY
});


function promiseFileUpload(fileUuid, fileName, localFilePath) {
	return new Promise(function (resolve, reject) {
		const metaData = {
			'Content-Type': 'application/octet-stream'
		}

		minioClient.fPutObject('uploads', `${fileUuid}/${fileName}`, localFilePath, metaData, function(error, etag) {
			if (error) {
				reject(error)
			} else {
				resolve()
			}
		});
	})
}


module.exports = {
	promiseFileUpload
}
