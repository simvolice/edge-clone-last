const MongoClient = require('mongodb').MongoClient;

const Logger = require('mongodb').Logger;
Logger.setLevel(process.env.MONGODB_LEVEL_LOG);


let state = {
    db: null
};

const connect = async () => {

    try {


        let client = await MongoClient.connect(process.env.MONGODB_DB_HOST, { useNewUrlParser: true });

        state.db = client.db(process.env.MONGODB_DB_NAME);




    } catch (err) {

        state.db = err;


    }

};


const getConnect = () => state.db;


module.exports = {

connect,
getConnect


};
