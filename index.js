const axios = require('axios');
const bodyParser = require('body-parser')
const cors = require('cors')
const express = require('express')
const request = require('request-promise-native')
const uuidv4 = require('uuid/v4')
const base64Img = require('base64-img');
const { exec } = require('child_process');
const fs = require('fs')
const basicAuth = require('express-basic-auth')
const Redis = require('ioredis');
const timeout = require('connect-timeout');
const dbConnect = require('./lib/dbConnect');
const MinioUtils = require('./lib/minioUtils.js')

const { _lg, _err } = require('single-line-logger')

const PORT = parseInt(process.env.EDGE_PORT)
const LOCAL_TMP_DIR = process.env.LOCAL_TMP_DIR
const CAMUNDA_URL = process.env.CAMUNDA_URL
const DICTIONARIES_BASE_URL = process.env.DICTIONARIES_BASE_URL
const KEYCLOAK_BASE_URL = process.env.KEYCLOAK_BASE_URL
const KEYCLOAK_REALM_ID = process.env.KEYCLOAK_REALM_ID
const KEYCLOAK_CLIENT_ID = process.env.KEYCLOAK_CLIENT_ID
const KEYCLOAK_CLIENT_SECRET = process.env.KEYCLOAK_CLIENT_SECRET
const BASIC_AUTH_ENABLED = (process.env.BASIC_AUTH_ENABLED === 'true')
const BASIC_AUTH_USER = process.env.BASIC_AUTH_USER
const BASIC_AUTH_PASSWORD = process.env.BASIC_AUTH_USER
const POSTGREST_AUTH_TOKEN = process.env.POSTGREST_AUTH_TOKEN



// Create & configure the express app
const app = express()
app.use(cors())
app.use(bodyParser.json({limit: '50mb'}))
app.use(timeout(180000))
if (BASIC_AUTH_ENABLED) {
    app.use(basicAuth({
        users: {
            [BASIC_AUTH_USER]: BASIC_AUTH_PASSWORD
        }
    }))
}

require('./routes')(app);
dbConnect.connect();


// Reusable redis connection
const redis = new Redis({
	host: process.env.REDIS_HOST
});




async function promiseLocation(locationId) {
    const response = await axios.get(`${DICTIONARIES_BASE_URL}/dictionaries/api/salesbigroup/real_estates`)
    const locations = response.data
    for (let location of locations) {
        if (location.guid == locationId) {
            return Promise.resolve(location)
        }
    }

    return Promise.resolve(null)
}


async function promiseBlock(locationId, blockId) {
    const response = await axios.get(`${DICTIONARIES_BASE_URL}/dictionaries/api/salesbigroup/blocks?id=${locationId}`)
    const blocks = response.data
    for (let block of blocks) {
        if (block.guid == blockId) {
            return Promise.resolve(block)
        }
    }

    return Promise.resolve(null)
}

async function promiseCreateFile(fileName, fileString) {
    return new Promise((resolve, reject) => {
        let writeStream = fs.createWriteStream(fileName);
        // write some data with a base64 encoding
        writeStream.write(fileString, 'base64');
        // the finish event is emitted when all data has been flushed from the stream
        writeStream.on('finish', () => {  
            console.log('wrote all data to file');
            resolve(`${fileName}`)
        });
        // close the stream
        writeStream.end(); 
    })
}

async function updateKeycloakBearer() {
    let response = await request({
        url: `${KEYCLOAK_BASE_URL}/auth/realms/${KEYCLOAK_REALM_ID}/protocol/openid-connect/token`,
        method: 'POST',
        auth: {
            user: KEYCLOAK_CLIENT_ID,
            pass: KEYCLOAK_CLIENT_SECRET
        },
        form: {
            'grant_type': 'client_credentials',
        }
    })
    let json = JSON.parse(response);
    let keycloakBearer = json.access_token
    return Promise.resolve(keycloakBearer)
}

app.post('/start-fixdefect-process', async function (req, res) {
    _lg('Handling request: ', req.body)

    try {
        // 1. auth with keycloak
        let keycloakBearer = await updateKeycloakBearer()

        // 2. build variables for Camunda REST using available fields
        const defectInfo = {
            "fields": {
                "requestStatus": req.body['status'],
                "requestDate": req.body['date'],
                "requestType": req.body['type_name'],
                "requestTypeId": req.body['type_id'],
                "requestAssurance": null,
                "requestCost": req.body['cost'],
                "requestDeadLine": req.body['deadline'],
                "requestDeadLineTime": req.body['deadline_time'],
                "requestContactPerson": req.body['contact_person_name'],
                "requestContactPersonNumber": req.body['contact_person_phone'],
                "requestFlatNumber": req.body['apt_number'],
                "requestNumber": req.body['id'],
                "requestPhotoUpload": [],
                "requestComment": req.body['comment'],
                "requestLocation": 'null',
                "requestBlock": 'null',
                "requestOwner": req.body['apt_owner_name'],
                "requestOwnerPhone": req.body['apt_owner_phone'],
                "requestPropertyName": req.body['service_property_name'],
                "requestSource": req.body['source'],
                "emptyLocation" : null,
            }
        }
        const requestId = req.body['id']
        const intIndex = req.body['index']
        const index = intIndex.toString()
        const intSourceCode = req.body['source_code']
        const sourceCode = intSourceCode.toString()
        const statusCode = req.body['status_code']
        // 3. Add location & block info
                
        if(req.body['real_estate_id'] == null || req.body['real_estate_id'] == ''){
            defectInfo.fields.requestLocation = 'null';
            defectInfo.fields.requestBlock = 'null';
            defectInfo.fields.emptyLocation = 'yes'
        } else {
            const locationId = req.body['real_estate_id']
            const blockId = req.body['real_estate_block_id']
            const location = await promiseLocation(locationId)
            defectInfo.fields.requestLocation = location

            const block = await promiseBlock(locationId, blockId)
            defectInfo.fields.requestBlock = block
            defectInfo.fields.emptyLocation = 'no'
        }

        // 4. Add photo
        const uuid = uuidv4()
        const photoBase64 = req.body['photo']

        if (photoBase64 && photoBase64.length > 0) {
            var localFilePath = base64Img.imgSync(photoBase64, LOCAL_TMP_DIR, `${uuid}`);

            const fileName = 'photo.jpg'
            const fileSize = fs.statSync(localFilePath)['size']

            await  MinioUtils.promiseFileUpload(uuid, fileName, localFilePath)
            defectInfo.fields.requestPhotoUpload = [
                {
                    fileName: fileName,
                    uuid: uuid,
                    type: 'image/jpeg',
                    size: fileSize,
                    author: 'WorkFlow Edge',
                    uploadDate: new Date(),
                }
            ]
            exec(`rm '${localFilePath}'`)
        }

        // 5. Post to camunda
        const response = await axios.post(
            `${CAMUNDA_URL}/camunda/rest/process-definition/key/fixdefect/start`,
            { 
                "variables": {
                    "defects": {
                        "value": JSON.stringify(defectInfo),
                        "type": "Json"
                    },
                    "internal":{
                        "value": "no",
                        "type": "String"
                    },
                    "id":{
                        "value": requestId,
                        "type":"String"
                    },
                    "index":{
                        "value": index,
                        "type": "String"
                    },
                    "source":{
                        "value": sourceCode,
                        "type": "String",
                    },
                    "statusFrom1C":{
                        "value": statusCode,
                        "type": "String",
                    },
                }
            },
            {
                headers: {
                    'Authorization': `Bearer ${keycloakBearer}`,
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            }
        )

        // 6. save to db
        const piId = response.data.id
        await redis.set(`integrations.bis.req_to_pi.${req.body['id']}-${req.body['index']}`, piId)

        // 7. send to BIS
        res.send({"status":"ok", "process_id" : piId})
    } catch(error) {
        _lg('Error in start-fixdefect-process:', error)
        res.status(500).send('Failed to start process')
    }
})

app.post('/remark-fixdefect-process', async function (req, res) {
    _lg('Handling request:', req.body)

    // auth with keycloak
    let keycloakBearer = await updateKeycloakBearer()

    try {
        // read mapping from db
        const piId = await redis.get(`integrations.bis.req_to_pi.${req.body['id']}-${req.body['index']}`)

        // send to camunda
        let response = await axios.post(

            `${CAMUNDA_URL}/camunda/rest/process-instance/${piId}/variables`,
            {
               
                "modifications":
                    {
                        "clientRate" : {"value": req.body['mark']},
                        "clientComment" : {"value":req.body['comment']},
                        "statusId" : {"value": req.body['statusId']},
                        "statusFrom1C" : {"value":req.body['statusId']}
                    }
                
            },
            {
                headers: {
                    'Authorization': `Bearer ${keycloakBearer}`,
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            }
        )

        res.send("OK")

    } catch (error) {
        _lg('Error in remark-fixdefect-process:', error)
        res.status(500).send('Failed to set remark')
    }
})





//base64 files to process-instance
app.post('/files-to-process-instance', async function(req, res) {
    try {
        let files = []
        // 1. auth with keycloak
        let keycloakBearer = await updateKeycloakBearer()
        const pid = req.body.id
        const file = req.body.file
        console.log('pid:',pid )
        console.log('file:',file )

        files = await axios.get(`${CAMUNDA_URL}/camunda/rest/process-instance/${pid}/variables?deserializeValues=false`,{ 
            headers: {
                'Authorization' : `Bearer ${keycloakBearer}`
            } 
        })
        .then(function (response) {
            if(response.data.hasOwnProperty('edgeFiles')){
                return  JSON.parse(response.data.edgeFiles.value)
            } else return []
        })
        .catch(function (error) {
            console.log('error', error)
            return []
        })
        // Add file to minio
        const uuid = uuidv4()
        const fileBase64 = file.FileString //req.body.file.FileString
        const fileName = file.Filename
        console.log('fileBase64', fileBase64)
        if (fileBase64 && fileBase64.length > 0) {
            const path = await promiseCreateFile(fileName,fileBase64)
            console.log('path', path)
            const fileSize = fs.statSync(path)['size']
            console.log('fileSize', fileSize)
            await  MinioUtils.promiseFileUpload(uuid, fileName, path)
            files.push({
                fileName: fileName,
                uuid: uuid,
                type: 'image/jpeg',
                size: fileSize,
                author: 'WorkFlow Edge',
                uploadDate: new Date(),
            })
            console.log('files', files)
            exec(`rm '${path}'`)
        }

        await axios.post(`${CAMUNDA_URL}/camunda/rest/process-instance/${pid}/variables`, {
            "modifications":{
                "edgeFiles":{
                    "value": JSON.stringify(files),
                    "type": "Json"
                }
            }
        },
        {
            headers: {
                'Authorization': `Bearer ${keycloakBearer}`,
                'Content-Type': 'application/json;charset=UTF-8'
            }
        })
        .catch(function (error) {
            console.log('error', error)
        })
        // 7. send to BIS
        res.status(200).send({"Success":true, "Message":""})
        // res.send({"status":"ok"})
    } catch(error) {
        _lg(`Error save file to process instance ${pid}:`, error)
        res.status(500).send({"Success":false, "Message":error})
    }
})

app.post('/start-SVR-matching-process', async function (req, res) {
    _lg('Handling request: ', req.body)
    try {
        // 1. auth with keycloak
        let keycloakBearer = await updateKeycloakBearer()

        // 2. build variables for Camunda REST using available fields
        const SRVInfo = {
            "documentId": req.body['Id'],
            "periodDate": new Date (req.body['PeriodDate']),
            "projectCode": req.body['ProjectCode'],
            "senderCompany": req.body['SenderCompany'],
            "recieverCompany": req.body['RecieverCompany'],
            "constructiveCode": req.body['ConstructiveCode'],
            "constructiveName": req.body['ConstructiveName'],
            "smetas": req.body['Smetas'],
        }

        
        const resp = await axios.get(
            `${DICTIONARIES_BASE_URL}/dictionaries/api/obj_group/`,
            {
                params: {
                    code: SRVInfo.projectCode
                }
            }
        )

        SRVInfo.objGroup = resp.data
        console.log('SRVInfo.objGroup', SRVInfo.objGroup)

        // 5. Post to camunda
        const response = await axios.post(
            `${CAMUNDA_URL}/camunda/rest/process-definition/key/SVR-matching/start`, {
                "businessKey" : SRVInfo.documentId, 
                "variables": {
                    "smetas": {
                        "value": JSON.stringify(SRVInfo.smetas),
                        "type": "Json"
                    },
                    "documentId":{
                        "value": SRVInfo.documentId,
                        "type": "String"
                    },
                    "periodDate":{
                        "value": SRVInfo.periodDate,
                        "type":"Date"
                    },
                    "projectCode":{
                        "value": SRVInfo.projectCode,
                        "type":"String"
                    },
                    "constructiveCode":{
                        "value": SRVInfo.constructiveCode,
                        "type": "String"
                    },
                    "constructiveName":{
                        "value": SRVInfo.constructiveName,
                        "type": "String"
                    },
                    "objectHeadGroup":{
                        "value": SRVInfo.objGroup ? SRVInfo.objGroup : `OBJ_${SRVInfo.projectCode}_ITR_HEAD`,
                        "type": "String"
                    },
                    "senderCompany":{
                        "value": JSON.stringify(SRVInfo.senderCompany),
                        "type": "Json"
                    },
                    "recieverCompany":{
                        "value": JSON.stringify(SRVInfo.recieverCompany),
                        "type": "Json"
                    },
                }
            },
            {
                headers: {
                    'Authorization': `Bearer ${keycloakBearer}`,
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            }
        )
        // 7. send to BIS
        res.status(200).send({"Success":true, "Message": "", "Id": response.data.id})
        // res.send({"status":"ok"})
    } catch(error) {
        _lg('Error in start-fixdefect-process:', error)
        res.status(500).send({"Success":false, "Message": error})
    }
})


app.post('/start-site-access-process', async function (req, res) {
    _lg('Handling request: ', req.body)
    
    try {
        // 1. auth with keycloak
        let keycloakBearer = await updateKeycloakBearer()
        console.log(keycloakBearer)
        // 2. build variables for Camunda REST using available fields
        // console.log('=========')
        // console.log(req.body)
        // console.log('=========')
        const companyName = req.body['companyName']
        const companyBIN = req.body['companyBIN']
        const employees = req.body['employees']
        const zone = req.body['zone']
        const location = req.body['location']
        const block = req.body['block']
        const workType = req.body['workType']
        const responsible_person = req.body['responsible_person']
        // 5. Post to camunda
        const response = await axios.post(
            `${CAMUNDA_URL}/camunda/rest/process-definition/key/site-access-request/start`,
            { 
                "variables": {
                    "companyName": {
                        "value": companyName,
                        "type": "String"
                    },
                    "companyBIN":{
                        "value": companyBIN,
                        "type": "String"
                    },
                    "employees":{
                        "value": JSON.stringify(employees),
                        "type":"Json"
                    },
                    "zone":{
                        "value": zone,
                        "type": "String"
                    },
                    "location":{
                        "value": JSON.stringify(location),
                        "type": "Json"
                    },
                    "block": {
                        "value": JSON.stringify(block),
                        "type": "Json"
                    },
                    "workType":{
                        "value": workType,
                        "type": "String"
                    },
                    "responsible_person":{
                        "value": JSON.stringify(responsible_person),
                        "type": "Json"
                    },
                }
            },
            {
                headers: {
                    'Authorization': `Bearer ${keycloakBearer}`,
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            }
        )

        // 6. save to db
        const piId = response.data.id
        // await redis.set(`integrations.bis.req_to_pi.${req.body['id']}-${req.body['index']}`, piId)

        // 7. send to BIS
        res.send({"status":"ok", "process_id" : piId})
    } catch(error) {
        _lg('Error in start-site-access-process:', error)
        res.status(500).send('Failed to start process')
    }
})

app.post('/start-site-access-process', async function (req, res) {
    _lg('Handling request: ', req.body)

    try {
        // 1. auth with keycloak
        let keycloakBearer = await updateKeycloakBearer()
        console.log(keycloakBearer)
        // 2. build variables for Camunda REST using available fields
        // console.log('=========')
        // console.log(req.body)
        // console.log('=========')
        const companyName = req.body['companyName']
        const companyBIN = req.body['companyBIN']
        const employees = req.body['employees']
        const zone = req.body['zone']
        const location = req.body['location']
        const block = req.body['block']
        const workType = req.body['workType']
        const responsible_person = req.body['responsible_person']
        // 5. Post to camunda
        const response = await axios.post(
            `${CAMUNDA_URL}/camunda/rest/process-definition/key/site-access-request/start`,
            {
                "variables": {
                    "companyName": {
                        "value": companyName,
                        "type": "String"
                    },
                    "companyBIN":{
                        "value": companyBIN,
                        "type": "String"
                    },
                    "employees":{
                        "value": JSON.stringify(employees),
                        "type":"Json"
                    },
                    "zone":{
                        "value": zone,
                        "type": "String"
                    },
                    "location":{
                        "value": JSON.stringify(location),
                        "type": "Json"
                    },
                    "block": {
                        "value": JSON.stringify(block),
                        "type": "Json"
                    },
                    "workType":{
                        "value": workType,
                        "type": "String"
                    },
                    "responsible_person":{
                        "value": JSON.stringify(responsible_person),
                        "type": "Json"
                    },
                }
            },
            {
                headers: {
                    'Authorization': `Bearer ${keycloakBearer}`,
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            }
        )

        // 6. save to db
        const piId = response.data.id
        // await redis.set(`integrations.bis.req_to_pi.${req.body['id']}-${req.body['index']}`, piId)

        // 7. send to BIS
        res.send({"status":"ok", "process_id" : piId})
    } catch(error) {
        _lg('Error in start-site-access-process:', error)
        res.status(500).send('Failed to start process')
    }
})



app.listen(PORT, () => _lg(`Workflow edge listening on port ${PORT}!`))







