FROM node:8.11

ENV \
    EDGE_PORT='3000' \
    LOCAL_TMP_DIR='/tmp/' \
    CAMUNDA_URL='http://wf.test' \
    DICTIONARIES_BASE_URL='http://wf.test' \
    KEYCLOAK_BASE_URL='http://wf.test' \
    KEYCLOAK_REALM_ID='bi-group' \
    KEYCLOAK_CLIENT_ID='camunda-external-task-client' \
    KEYCLOAK_CLIENT_SECRET='218ee2be-d615-42ab-83eb-a8286405255e' \
    MINIO_HOST='localhost' \
    MINIO_PORT='9000' \
    MINIO_USE_SSL='false' \
    MINIO_ACCESS_KEY='AKIAIOSFODNN7EXAMPLE' \
    MINIO_SECRET_KEY='wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY' \
    BASIC_AUTH_ENABLED='false' \
    BASIC_AUTH_USER='test' \
    BASIC_AUTH_PASSWORD='test' \
    REDIS_HOST='localhost' \
    POSTGREST_URL='http://test.wf.bein.tech:3000'
ENV  MONGODB_LEVEL_LOG=error
ENV  MONGODB_DB_HOST=mongodb://mongodb:27017
ENV  MONGODB_DB_NAME=integrations

WORKDIR /app

COPY package.json /app
COPY index.js /app
COPY lib /app/lib
COPY integrations /app/integrations
COPY routes /app/routes
COPY tests /app/tests

RUN npm install

EXPOSE 3000
CMD node index.js
