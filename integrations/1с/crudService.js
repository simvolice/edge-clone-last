const dbConnect = require('../../lib/dbConnect');



async function getData(data, tableName) {


    try {

        const col = dbConnect.getConnect().collection(tableName);

        const result = await col.find({});


        return result;


    } catch (err) {

        return err;
    }

}



async function insertOneInTable(data, tableName) {


    try {

        const col = dbConnect.getConnect().collection(tableName);


        const result = await col.insertOne(data);



        if (result.result.hasOwnProperty("ok")) {


            if (result.result.ok === 1) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 1;

        }

    } catch (err) {

        return 1;
    }

}


async function insertServicePurchasePlan(data, tableName) {


    try {


        const col = dbConnect.getConnect().collection(tableName);

        col.createIndex({Guid: 1}, {unique: true});


        const result = await col.insertMany(data);


        if (result.result.hasOwnProperty("ok")) {


            if (result.result.ok === 1) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 1;

        }

    } catch (err) {


        if (err.code === 11000) {
            return err.code;

        } else {
            return 1;
        }


    }

}


async function updatePurchasePlanByGuid(dataObj, tableName) {


    try {


        const col = dbConnect.getConnect().collection(tableName);


        const result = await col.findOneAndUpdate({Guid: dataObj.Guid}, {
            $set: {


                "Date": dataObj.Date,
                "Number": dataObj.Number,
                "Posted": dataObj.Posted,
                "NumberRequest": dataObj.NumberRequest,
                "CommentToRequest": dataObj.CommentToRequest,
                "Comment": dataObj.Comment,
                "Organization": dataObj.Organization,
                "Project": dataObj.Project,
                "Subproject": dataObj.Subproject,
                "Constructive": dataObj.Constructive,
                "Responsible": dataObj.Responsible,
                "Author": dataObj.Author,
                "Period": dataObj.Period,
                "Executor": dataObj.Executor,
                "Products": dataObj.Products


            }
        });


        if (result.ok === 1) {
            return 0;
        } else {
            return 1;
        }


    } catch (err) {


        return 1;


    }

}


async function updateIndicativesByGuid(dataObj, tableName) {


    try {


        const col = dbConnect.getConnect().collection(tableName);


        const result = await col.findOneAndUpdate({"Nomenclature.Guid": dataObj.Nomenclature.Guid}, {
            $set: {

                "Period": dataObj.Period,
                "Nomenclature": dataObj.Nomenclature,
                "Country": dataObj.Country,
                "City": dataObj.City,
                "Unit": dataObj.Unit,
                "PriceWithVAT": dataObj.PriceWithVAT,
                "PriceWithoutVAT": dataObj.PriceWithoutVAT


            }
        });


        if (result.ok === 1) {
            return 0;
        } else {
            return 1;
        }


    } catch (err) {


        return 1;


    }

}


async function insertServicePurchasePlanHistorical(data, tableName) {


    try {


        const col = dbConnect.getConnect().collection(tableName);


        await col.insertMany(data);

        return 0;

    } catch (err) {

        return 1;
    }

}


async function getprojectObjByGuid(guid, tableName) {


    try {

        const col = dbConnect.getConnect().collection(tableName);


        let result = await col.findOne({guid: guid});


        if (result !== null) {
            return result;
        } else {
            return 1;
        }


    } catch (err) {

        return 1;
    }

}

async function insertServiceIndicative(data, tableName) {


    try {


        const col = dbConnect.getConnect().collection(tableName);

        col.createIndex({"Nomenclature.Guid": 1}, {unique: true});


        const result = await col.insertMany(data);


        if (result.result.hasOwnProperty("ok")) {


            if (result.result.ok === 1) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 1;

        }

    } catch (err) {


        if (err.code === 11000) {
            return err.code;

        } else {
            return 1;
        }


    }

}



async function getProjectObjByProjectCode(project_code, tableName) {


    try {

        const col = dbConnect.getConnect().collection(tableName);


        let result = await col.find({"Project.Guid": project_code}).toArray();


        if (result.length !== 0) {
            return result;
        } else {
            return 1;
        }


    } catch (err) {

        return 1;
    }

}



async function getIndecativesByArrGuid(arrGuids, tableName) {


    try {

        const col = dbConnect.getConnect().collection(tableName);


        let result = await col.find({ "Nomenclature.Guid": { $in: arrGuids } }).toArray();


        if (result.length !== 0) {
            return result;
        } else {
            return 1;
        }


    } catch (err) {

        return 1;
    }

}


async function insertServiceUspWorks(data, tableName) {


    try {


        const col = dbConnect.getConnect().collection(tableName);

        col.createIndex({Guid: 1}, {unique: true});


        const result = await col.insertOne(data);


        if (result.result.hasOwnProperty("ok")) {


            if (result.result.ok === 1) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 1;

        }

    } catch (err) {


        if (err.code === 11000) {
            return err.code;

        } else {
            return 1;
        }


    }

}

async function updateUspWorksByGuid(dataObj, tableName) {


    try {


        const col = dbConnect.getConnect().collection(tableName);


        const result = await col.findOneAndUpdate({Guid: dataObj.Guid}, {
            $set: {


                "Date": dataObj.Date,
                "Number": dataObj.Number,
                "Comment": dataObj.Comment,
                "Project": dataObj.Project,
                "Organization": dataObj.Organization,
                "Works": dataObj.Works



            }
        });


        if (result.ok === 1) {
            return 0;
        } else {
            return 1;
        }


    } catch (err) {


        return 1;


    }

}


module.exports = {
    insertServicePurchasePlan,
    updatePurchasePlanByGuid,
    insertServicePurchasePlanHistorical,
    getprojectObjByGuid,
    insertServiceIndicative,
    updateIndicativesByGuid,
    insertOneInTable,
    getProjectObjByProjectCode,
    getIndecativesByArrGuid,
    insertServiceUspWorks,
    updateUspWorksByGuid,
    getData
};