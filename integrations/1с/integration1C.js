const express = require('express');
const router = express.Router();

const CrudService = require('./crudService');


/**
 * 1С не может высылать методы PUT, необходимо всегда все обрабатывать в POST.
 * Структура из 1С, всегда дерево.
 * При обновление данных, всегда приходит все дерево, 1С не может высылать только изменненую часть.
 */
router.post('/purchase-plan', async (req, res) => {


    if (Object.keys(req.body).length === 0) {

        res.json({code: 1});

    } else {


        if (req.body.hasOwnProperty("PurchasePlan")) {


            await CrudService.insertServicePurchasePlanHistorical(req.body.PurchasePlan, "purchase_plan_historical");
            const resultCode = await CrudService.insertServicePurchasePlan(req.body.PurchasePlan, "purchase_plan");


            if (resultCode === 11000) {
                let resultCodeUpd = 0;

                for (let i = 0; i < req.body.PurchasePlan.length; i++) {
                    const purchasePlanElement = req.body.PurchasePlan[i];
                    resultCodeUpd = await CrudService.updatePurchasePlanByGuid(purchasePlanElement, "purchase_plan");

                }
                res.json({code: resultCodeUpd});
            } else {
                res.json({code: resultCode});
            }


        } else {

            res.json({code: 1});
        }


    }

});


router.get('/getcode', async (req, res) => {


    if (Object.keys(req.query).length === 0) {

        res.json({code: 1});

    } else {


        if (req.query.hasOwnProperty("guid")) {


            const resultCode = await CrudService.getprojectObjByGuid(req.query.guid, "codes");


            if (resultCode !== 1) {
                res.json({code: 0, data: resultCode});
            } else {
                res.json({code: 1});
            }


        } else {

            res.json({code: 1});
        }


    }

});


router.get('/getprojects', async (req, res) => {


    if (Object.keys(req.query).length === 0) {

        res.json({code: 1});

    } else {


        if (req.query.hasOwnProperty("project_code")) {


            const resultCode = await CrudService.getProjectObjByProjectCode(req.query.project_code, "purchase_plan");

            if (resultCode !== 1) {
                res.json({code: 0, data: resultCode});

            } else {
                res.json({code: 1});

            }


        } else {

            res.json({code: 1});
        }


    }

});


router.post('/savedb', async (req, res) => {


    if (Object.keys(req.body).length === 0) {

        res.json({code: 1});

    } else {


        if (req.body.hasOwnProperty("tableName")) {


            const resultCode = await CrudService.insertOneInTable(req.body.data, req.body.tableName);


            res.json({code: resultCode});

        } else {

            res.json({code: 1});
        }


    }


});


router.post('/material-supply/post-indicative-prices', async (req, res) => {


    if (Object.keys(req.body).length === 0) {


        res.json({code: 1});

    } else {


        if (req.body.hasOwnProperty("CapitalCostsPrices")) {


            let result = await CrudService.insertServiceIndicative(req.body.CapitalCostsPrices, "indicatives");


            if (result === 11000) {
                let resultCodeUpd = 0;

                for (let i = 0; i < req.body.CapitalCostsPrices.length; i++) {
                    const capitalCostsPricesElement = req.body.CapitalCostsPrices[i];
                    resultCodeUpd = await CrudService.updateIndicativesByGuid(capitalCostsPricesElement, "indicatives");

                }
                res.json({code: resultCodeUpd});
            } else {
                res.json({code: result});
            }


        } else {

            res.json({code: 1});
        }


    }

});


router.get('/indicatives', async (req, res) => {


    if (Object.keys(req.query).length === 0) {

        res.json({code: 1});

    } else {


        if (req.query.hasOwnProperty("guids")) {


            let parseArr = JSON.parse(req.query.guids);

            const resultCode = await CrudService.getIndecativesByArrGuid(parseArr, "indicatives");

            if (resultCode !== 1) {
                res.json({code: 0, data: resultCode});

            } else {
                res.json({code: 1});

            }


        } else {

            res.json({code: 1});
        }


    }

});


router.post('/uspworks', async (req, res) => {


    if (Object.keys(req.body).length === 0) {

        res.json({code: 1});

    } else {


        if (req.body.hasOwnProperty("Documents")) {


            const resultCode = await CrudService.insertServiceUspWorks(req.body.Documents, "usp_works");


            if (resultCode === 11000) {

                let resultCodeUpd = await CrudService.updateUspWorksByGuid(req.body.Documents, "usp_works");


                res.json({code: resultCodeUpd});
            } else {
                res.json({code: resultCode});
            }


        } else {

            res.json({code: 1});
        }


    }


});


router.get('/uspresources', async (req, res) => {


    res.json({code: 0});



});


module.exports = router;