const test = require("ava");
const mockData = require("./mockData/mock");


const ReaderService = require("../lib/reader_integration");


test('Тест на ReaderService, на возврат кода ошибки', async t => {
    const result = await ReaderService.readService({test: 555}, "http://test-opera.bi.group:3000","purchase_plan_no_sql", 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYXNzZXRzIn0.Qd-T_YuB5vM2H2z0a9q3vq1_PF7Gl6m2KQ3LdcXjP4I');
    t.is(result, 1);
});


test('Тест на ReaderService, на возврат кода успеха', async t => {


    let data = {
        project_code: mockData.PurchasePlan[0].Project.Guid,
        data_json: mockData
    };
    const result = await ReaderService.readService(data, "http://test-opera.bi.group:3000","purchase_plan_no_sql", 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYXNzZXRzIn0.Qd-T_YuB5vM2H2z0a9q3vq1_PF7Gl6m2KQ3LdcXjP4I');
    t.is(result, 0);
});