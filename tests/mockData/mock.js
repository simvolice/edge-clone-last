    const mockData = {
        "PurchasePlan": [
            {
                "Guid": "6ea5cf58-0773-11e8-80c9-001dd8b71c93",
                "Date": "2018-02-01T23:15:00Z",
                "Number": "TNH000001",
                "Posted": true,
                "NumberRequest": 0,
                "CommentToRequest": "",
                "Comment": "",
                "Organization": {
                    "Guid": "9209b2ef-1973-11e1-9810-9c8e994b2add",
                    "Name": "Town House ТОО",
                    "RegionForIndicativePrices": "ASTANA",
                    "HoldingNumber": "Холдинг2"
                },
                "Project": {
                    "Guid": "11dbc56a-b682-11e4-aece-40f2e9cd30ba",
                    "Name": "Камал 4 - Опер. деят.",
                    "Completed": false,
                    "ProjectStartDate": "0001-01-01T00:00:00Z",
                    "ProjectEndDate": "0001-01-01T00:00:00Z",
                    "ProjectManager": {
                        "Guid": "00000000-0000-0000-0000-000000000000",
                        "Name": ""
                    },
                    "CheckPlanPA": false,
                    "DateOfMonitoringTheControl": "0001-01-01T00:00:00Z"
                },
                "Subproject": {
                    "Guid": "00000000-0000-0000-0000-000000000000",
                    "Name": ""
                },
                "Constructive": {
                    "Guid": "00000000-0000-0000-0000-000000000000",
                    "Name": ""
                },
                "Responsible": {
                    "Guid": "3c009404-4522-11e3-a739-e4115bceaa2d",
                    "Name": "Мұхамедрахман Азат Қанатұлы"
                },
                "Author": {
                    "Guid": "e496614a-659c-11e4-8350-e4115bceaa2c",
                    "Name": "TNH_Мұхамедрахман_Азат_Канатұлы"
                },
                "Period": "2018-02-01T00:00:00Z",
                "Executor": {
                    "Guid": "fc9e7095-389b-11e4-a60c-e4115bceaa2c",
                    "Name": "Рашиев Ринат Кудусулы"
                },
                "Products": [
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e8de-ebc6-11e3-b049-002590952079",
                            "Name": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 100",
                            "Kod": "CNT00002066",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-94",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-94",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 100",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e897-ebc6-11e3-b049-002590952079",
                            "Name": "Утеплитель",
                            "Kod": "CNT00001995",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-23",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-23",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Утеплитель",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "64ff0164-ebc6-11e3-b049-002590952079",
                            "Name": "Пленка оберточная гидроизоляционная ПДБ, толщина 0,55 мм, (ТУ 21-2749-76)",
                            "Kod": "CNT00000642",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-504",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-504",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пленка оберточная гидроизоляционная ПДБ, толщина 0,55 мм, (ТУ 21-2749-76)",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "b92d1024-4021-11e5-86c2-40f2e9cd30ba",
                            "Name": "Ступени лестничные сборные железобетонные ЛС-15-Б",
                            "Kod": "HL200007291",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1502KAM-30-13-780",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1502KAM-30-13-780",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Ступени лестничные сборные железобетонные ЛС-15-Б",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f46-ebc7-11e3-b049-002590952079",
                            "Name": "Керамзитобетон",
                            "Kod": "CNT00009732",
                            "CheckPlanPA": false,
                            "EstimatedCode": "30-1-9999",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-1-9999",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Керамзитобетон",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "20b718f8-65a3-11e5-af40-40f2e9cd30ba",
                            "Name": "Гидроизоляционная ветрозащитная паропроницаемая мембрана Изоспан А",
                            "Kod": "HL200007455",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-9326",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-9326",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Гидроизоляционная ветрозащитная паропроницаемая мембрана Изоспан А",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "6d8e4141-fa8e-11e2-80cd-1cc1de028fc2",
                            "Name": "дресва",
                            "Kod": "30-8-108   ",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b8957f3b-ad0a-11e4-bc77-e4115bceaa2c",
                            "Name": "куб"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "дресва",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "782dc5b5-ebc6-11e3-b049-002590952079",
                            "Name": "Пена монтажная для герметизации стыков, баллончик емкостью 750 мл",
                            "Kod": "CNT00001300",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-1163",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-1163",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пена монтажная для герметизации стыков, баллончик емкостью 750 мл",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "f2de8c78-57db-11e7-8a6a-40f2e9cd30ba",
                            "Name": "Праймер битумный Bitumast TM",
                            "Kod": "HL200021365",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2810",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "7eb1013e-1464-11df-85d4-505054503030",
                            "Name": "кг"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2810",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Праймер битумный Bitumast TM",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "83f06c85-447e-11e4-ba7f-40f2e9cd30ba",
                            "Name": "Счетчик холодной воды с радиомодулем, класса точности С Flodis d=15мм",
                            "Kod": "HL200000134",
                            "CheckPlanPA": false,
                            "EstimatedCode": "11-2-9937",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "11-2-9937",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Счетчик холодной воды с радиомодулем, класса точности С Flodis d=15мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "782dc62a-ebc6-11e3-b049-002590952079",
                            "Name": "Геомембрана (СТО ТОО 50404263-032008), гм KGS 1,0",
                            "Kod": "CNT00001417",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-1392",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-1392",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Геомембрана (СТО ТОО 50404263-032008), гм KGS 1,0",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "ca51c636-57dc-11e7-8a6a-40f2e9cd30ba",
                            "Name": "Счетчик горячей воды крыльчатый с радиомодулем, класса точности В Unimag Cyble d=15мм, в комплекте с фильтром",
                            "Kod": "HL200021462",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1605NOV-11-2-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "693cc6a2-9fe2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "комплект"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1605NOV-11-2-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Счетчик горячей воды крыльчатый с радиомодулем, класса точности В Unimag Cyble d=15мм, в комплекте с фильтром",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f48-ebc7-11e3-b049-002590952079",
                            "Name": "Раствор",
                            "Kod": "CNT00009734",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-2-1",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-2-1",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Раствор",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e91c-ebc6-11e3-b049-002590952079",
                            "Name": "Маты теплоизоляционные URSA, марка М 11-50",
                            "Kod": "CNT00002128",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-156",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fb0-17b7-11df-9177-505054503030",
                            "Name": "Рулон"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-156",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Маты теплоизоляционные URSA, марка М 11-50",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "7e748b47-ebc6-11e3-b049-002590952079",
                            "Name": "Листы гипсокартонные ГКЛВ, ГОСТ 6266-97, толщина 9,5 мм",
                            "Kod": "CNT00001600",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2191",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2191",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Листы гипсокартонные ГКЛВ, ГОСТ 6266-97, толщина 9,5 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e8dd-ebc6-11e3-b049-002590952079",
                            "Name": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 75",
                            "Kod": "CNT00002065",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-93",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-93",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 75",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "9e679b02-ebc6-11e3-b049-002590952079",
                            "Name": "Сетка арматурная",
                            "Kod": "CNT00002978",
                            "CheckPlanPA": true,
                            "EstimatedCode": "4-1-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "4-1-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Сетка арматурная",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "7e748b50-ebc6-11e3-b049-002590952079",
                            "Name": "Рубероид подкладочный с пылевидной посыпкой РППс-250 ГОСТ 10923 93",
                            "Kod": "CNT00001609",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2259",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2259",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рубероид подкладочный с пылевидной посыпкой РППс-250 ГОСТ 10923 93",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f45-ebc7-11e3-b049-002590952079",
                            "Name": "Пескобетон МБ100",
                            "Kod": "CNT00009731",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-1-9949",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-1-9949",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пескобетон МБ100",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "ebe88052-ee64-11e4-86c2-40f2e9cd30ba",
                            "Name": "Кирпич керамический полнотелый полуторный (ГОСТ 530-95*), 250х120х88мм, М-125",
                            "Kod": "HL200005540",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-6-9990",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "6e57f8cf-825e-11e3-8b9f-002590952079",
                            "Name": "1000 шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-6-9990",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Кирпич керамический полнотелый полуторный (ГОСТ 530-95*), 250х120х88мм, М-125",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "4ed0951f-ebc7-11e3-b049-002590952079",
                            "Name": "Песок керамзитовый, М-800",
                            "Kod": "CNT00010053",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-8-102",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-8-102",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Песок керамзитовый, М-800",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "0388d9e8-ebc7-11e3-b049-002590952079",
                            "Name": "Зонты круглые из листовой стали марки 3К250 для вентиляционных шахт, d=250 мм",
                            "Kod": "CNT00006926",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-22",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-22",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К250 для вентиляционных шахт, d=250 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "0388d9e7-ebc7-11e3-b049-002590952079",
                            "Name": "Зонты круглые из листовой стали марки 3К200 для вентиляционных шахт, d=200 мм",
                            "Kod": "CNT00006925",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-21",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-21",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К200 для вентиляционных шахт, d=200 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "5b13bc23-ceb4-11e7-a678-40f2e9cd30ba",
                            "Name": "Зонты круглые из листовой стали марки 3К-125 для вентиляционных шахт, d=125мм",
                            "Kod": "HL200025173",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-9030",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "e0e4a60f-eb48-11e1-8e1f-9c8e994b2add",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-9030",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К-125 для вентиляционных шахт, d=125мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "71ab7e08-ebc6-11e3-b049-002590952079",
                            "Name": "Топливо моторное, марка ДТ",
                            "Kod": "CNT00001120",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-983",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "cde7cd8d-ebc3-11e3-b049-002590952079",
                            "Name": "т"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-983",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Топливо моторное, марка ДТ",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "f240edb8-be72-11e5-8a95-40f2e9cd30ba",
                            "Name": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭПП 4,0",
                            "Kod": "HL200008234",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-8848",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "4ed4450c-d37f-11e4-818e-68b599b09899",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-8848",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭПП 4,0",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "3c017b9c-8434-11e4-86f5-9c8e994b2add",
                            "Name": "Анимометр",
                            "Kod": "INC00004790",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Анимометр",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c09024-ebc7-11e3-b049-002590952079",
                            "Name": "Щебень",
                            "Kod": "CNT00009954",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-8-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-8-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Щебень",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "22625229-4468-11e4-ba7f-40f2e9cd30ba",
                            "Name": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭКП 2,6",
                            "Kod": "HL200000006",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-9693",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-9693",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭКП 2,6",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "04541293-a819-11e7-8630-40f2e9cd30ba",
                            "Name": "Гидрофобизатор Гидротекс-Ф",
                            "Kod": "HL200024133",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-3176",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947f13-17b7-11df-9177-505054503030",
                            "Name": "л"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-3176",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Гидрофобизатор Гидротекс-Ф",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-01-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "3b7ad83c-17c1-11df-9177-505054503030",
                            "Name": "Минплита",
                            "Kod": "00000003027",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Минплита",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "7e748b47-ebc6-11e3-b049-002590952079",
                            "Name": "Листы гипсокартонные ГКЛВ, ГОСТ 6266-97, толщина 9,5 мм",
                            "Kod": "CNT00001600",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2191",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 400,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2191",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Листы гипсокартонные ГКЛВ, ГОСТ 6266-97, толщина 9,5 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "5b13bc23-ceb4-11e7-a678-40f2e9cd30ba",
                            "Name": "Зонты круглые из листовой стали марки 3К-125 для вентиляционных шахт, d=125мм",
                            "Kod": "HL200025173",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-9030",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "e0e4a60f-eb48-11e1-8e1f-9c8e994b2add",
                            "Name": "шт"
                        },
                        "Count": 30000,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-9030",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К-125 для вентиляционных шахт, d=125мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "22625229-4468-11e4-ba7f-40f2e9cd30ba",
                            "Name": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭКП 2,6",
                            "Kod": "HL200000006",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-9693",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 1000,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-9693",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭКП 2,6",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "ebe88052-ee64-11e4-86c2-40f2e9cd30ba",
                            "Name": "Кирпич керамический полнотелый полуторный (ГОСТ 530-95*), 250х120х88мм, М-125",
                            "Kod": "HL200005540",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-6-9990",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "6e57f8cf-825e-11e3-8b9f-002590952079",
                            "Name": "1000 шт"
                        },
                        "Count": 30,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-6-9990",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Кирпич керамический полнотелый полуторный (ГОСТ 530-95*), 250х120х88мм, М-125",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "4ed0951f-ebc7-11e3-b049-002590952079",
                            "Name": "Песок керамзитовый, М-800",
                            "Kod": "CNT00010053",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-8-102",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 400,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-8-102",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Песок керамзитовый, М-800",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "9e679b02-ebc6-11e3-b049-002590952079",
                            "Name": "Сетка арматурная",
                            "Kod": "CNT00002978",
                            "CheckPlanPA": true,
                            "EstimatedCode": "4-1-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 500,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "4-1-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Сетка арматурная",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f45-ebc7-11e3-b049-002590952079",
                            "Name": "Пескобетон МБ100",
                            "Kod": "CNT00009731",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-1-9949",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 100,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-1-9949",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пескобетон МБ100",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "6d8e4141-fa8e-11e2-80cd-1cc1de028fc2",
                            "Name": "дресва",
                            "Kod": "30-8-108   ",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b8957f3b-ad0a-11e4-bc77-e4115bceaa2c",
                            "Name": "куб"
                        },
                        "Count": 100,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "дресва",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "782dc5b5-ebc6-11e3-b049-002590952079",
                            "Name": "Пена монтажная для герметизации стыков, баллончик емкостью 750 мл",
                            "Kod": "CNT00001300",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-1163",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 40,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-1163",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пена монтажная для герметизации стыков, баллончик емкостью 750 мл",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e8de-ebc6-11e3-b049-002590952079",
                            "Name": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 100",
                            "Kod": "CNT00002066",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-94",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 20,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-94",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 100",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "7e748b50-ebc6-11e3-b049-002590952079",
                            "Name": "Рубероид подкладочный с пылевидной посыпкой РППс-250 ГОСТ 10923 93",
                            "Kod": "CNT00001609",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2259",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 20,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2259",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рубероид подкладочный с пылевидной посыпкой РППс-250 ГОСТ 10923 93",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e8dd-ebc6-11e3-b049-002590952079",
                            "Name": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 75",
                            "Kod": "CNT00002065",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-93",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 100,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-93",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 75",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f46-ebc7-11e3-b049-002590952079",
                            "Name": "Керамзитобетон",
                            "Kod": "CNT00009732",
                            "CheckPlanPA": false,
                            "EstimatedCode": "30-1-9999",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 1000,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-1-9999",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Керамзитобетон",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "0388d9e7-ebc7-11e3-b049-002590952079",
                            "Name": "Зонты круглые из листовой стали марки 3К200 для вентиляционных шахт, d=200 мм",
                            "Kod": "CNT00006925",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-21",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 40000,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-21",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К200 для вентиляционных шахт, d=200 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "3b7ad83c-17c1-11df-9177-505054503030",
                            "Name": "Минплита",
                            "Kod": "00000003027",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 80,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Минплита",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "3c017b9c-8434-11e4-86f5-9c8e994b2add",
                            "Name": "Анимометр",
                            "Kod": "INC00004790",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 1,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Анимометр",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e91c-ebc6-11e3-b049-002590952079",
                            "Name": "Маты теплоизоляционные URSA, марка М 11-50",
                            "Kod": "CNT00002128",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-156",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fb0-17b7-11df-9177-505054503030",
                            "Name": "Рулон"
                        },
                        "Count": 20,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-156",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Маты теплоизоляционные URSA, марка М 11-50",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "782dc62a-ebc6-11e3-b049-002590952079",
                            "Name": "Геомембрана (СТО ТОО 50404263-032008), гм KGS 1,0",
                            "Kod": "CNT00001417",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-1392",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 1600,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-1392",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Геомембрана (СТО ТОО 50404263-032008), гм KGS 1,0",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "f240edb8-be72-11e5-8a95-40f2e9cd30ba",
                            "Name": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭПП 4,0",
                            "Kod": "HL200008234",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-8848",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "4ed4450c-d37f-11e4-818e-68b599b09899",
                            "Name": "м2"
                        },
                        "Count": 1000,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-8848",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭПП 4,0",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "64ff0164-ebc6-11e3-b049-002590952079",
                            "Name": "Пленка оберточная гидроизоляционная ПДБ, толщина 0,55 мм, (ТУ 21-2749-76)",
                            "Kod": "CNT00000642",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-504",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fb0-17b7-11df-9177-505054503030",
                            "Name": "Рулон"
                        },
                        "Count": 20,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-504",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пленка оберточная гидроизоляционная ПДБ, толщина 0,55 мм, (ТУ 21-2749-76)",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "ca51c636-57dc-11e7-8a6a-40f2e9cd30ba",
                            "Name": "Счетчик горячей воды крыльчатый с радиомодулем, класса точности В Unimag Cyble d=15мм, в комплекте с фильтром",
                            "Kod": "HL200021462",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1605NOV-11-2-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "693cc6a2-9fe2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "комплект"
                        },
                        "Count": 153,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1605NOV-11-2-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Счетчик горячей воды крыльчатый с радиомодулем, класса точности В Unimag Cyble d=15мм, в комплекте с фильтром",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "b92d1024-4021-11e5-86c2-40f2e9cd30ba",
                            "Name": "Ступени лестничные сборные железобетонные ЛС-15-Б",
                            "Kod": "HL200007291",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1502KAM-30-13-780",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 50,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1502KAM-30-13-780",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Ступени лестничные сборные железобетонные ЛС-15-Б",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "20b718f8-65a3-11e5-af40-40f2e9cd30ba",
                            "Name": "Гидроизоляционная ветрозащитная паропроницаемая мембрана Изоспан А",
                            "Kod": "HL200007455",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-9326",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 2000,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-9326",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Гидроизоляционная ветрозащитная паропроницаемая мембрана Изоспан А",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "04541293-a819-11e7-8630-40f2e9cd30ba",
                            "Name": "Гидрофобизатор Гидротекс-Ф",
                            "Kod": "HL200024133",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-3176",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947f13-17b7-11df-9177-505054503030",
                            "Name": "л"
                        },
                        "Count": 100,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-3176",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Гидрофобизатор Гидротекс-Ф",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "f2de8c78-57db-11e7-8a6a-40f2e9cd30ba",
                            "Name": "Праймер битумный Bitumast TM",
                            "Kod": "HL200021365",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2810",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "7eb1013e-1464-11df-85d4-505054503030",
                            "Name": "кг"
                        },
                        "Count": 1000,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2810",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Праймер битумный Bitumast TM",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f48-ebc7-11e3-b049-002590952079",
                            "Name": "Раствор",
                            "Kod": "CNT00009734",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-2-1",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 50,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-2-1",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Раствор",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "83f06c85-447e-11e4-ba7f-40f2e9cd30ba",
                            "Name": "Счетчик холодной воды с радиомодулем, класса точности С Flodis d=15мм",
                            "Kod": "HL200000134",
                            "CheckPlanPA": false,
                            "EstimatedCode": "11-2-9937",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 153,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "11-2-9937",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Счетчик холодной воды с радиомодулем, класса точности С Flodis d=15мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e897-ebc6-11e3-b049-002590952079",
                            "Name": "Утеплитель",
                            "Kod": "CNT00001995",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-23",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 100,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-23",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Утеплитель",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "71ab7e08-ebc6-11e3-b049-002590952079",
                            "Name": "Топливо моторное, марка ДТ",
                            "Kod": "CNT00001120",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-983",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "cde7cd8d-ebc3-11e3-b049-002590952079",
                            "Name": "т"
                        },
                        "Count": 10,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-983",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Топливо моторное, марка ДТ",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c09024-ebc7-11e3-b049-002590952079",
                            "Name": "Щебень",
                            "Kod": "CNT00009954",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-8-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 100,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-8-2",
                        "Note": "ДРЕСВА",
                        "MethodKralich": "",
                        "FullName": "Щебень",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-02-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "0388d9e8-ebc7-11e3-b049-002590952079",
                            "Name": "Зонты круглые из листовой стали марки 3К250 для вентиляционных шахт, d=250 мм",
                            "Kod": "CNT00006926",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-22",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 5000,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-22",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К250 для вентиляционных шахт, d=250 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "5b13bc23-ceb4-11e7-a678-40f2e9cd30ba",
                            "Name": "Зонты круглые из листовой стали марки 3К-125 для вентиляционных шахт, d=125мм",
                            "Kod": "HL200025173",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-9030",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "e0e4a60f-eb48-11e1-8e1f-9c8e994b2add",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-9030",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К-125 для вентиляционных шахт, d=125мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "f2de8c78-57db-11e7-8a6a-40f2e9cd30ba",
                            "Name": "Праймер битумный Bitumast TM",
                            "Kod": "HL200021365",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2810",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "7eb1013e-1464-11df-85d4-505054503030",
                            "Name": "кг"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2810",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Праймер битумный Bitumast TM",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "7e748b47-ebc6-11e3-b049-002590952079",
                            "Name": "Листы гипсокартонные ГКЛВ, ГОСТ 6266-97, толщина 9,5 мм",
                            "Kod": "CNT00001600",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2191",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2191",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Листы гипсокартонные ГКЛВ, ГОСТ 6266-97, толщина 9,5 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "71ab7e08-ebc6-11e3-b049-002590952079",
                            "Name": "Топливо моторное, марка ДТ",
                            "Kod": "CNT00001120",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-983",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "cde7cd8d-ebc3-11e3-b049-002590952079",
                            "Name": "т"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-983",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Топливо моторное, марка ДТ",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "83f06c85-447e-11e4-ba7f-40f2e9cd30ba",
                            "Name": "Счетчик холодной воды с радиомодулем, класса точности С Flodis d=15мм",
                            "Kod": "HL200000134",
                            "CheckPlanPA": false,
                            "EstimatedCode": "11-2-9937",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "11-2-9937",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Счетчик холодной воды с радиомодулем, класса точности С Flodis d=15мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "f240edb8-be72-11e5-8a95-40f2e9cd30ba",
                            "Name": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭПП 4,0",
                            "Kod": "HL200008234",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-8848",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "4ed4450c-d37f-11e4-818e-68b599b09899",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-8848",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭПП 4,0",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e8de-ebc6-11e3-b049-002590952079",
                            "Name": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 100",
                            "Kod": "CNT00002066",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-94",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-94",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 100",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "782dc62a-ebc6-11e3-b049-002590952079",
                            "Name": "Геомембрана (СТО ТОО 50404263-032008), гм KGS 1,0",
                            "Kod": "CNT00001417",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-1392",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-1392",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Геомембрана (СТО ТОО 50404263-032008), гм KGS 1,0",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e8dd-ebc6-11e3-b049-002590952079",
                            "Name": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 75",
                            "Kod": "CNT00002065",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-93",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-93",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 75",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c09024-ebc7-11e3-b049-002590952079",
                            "Name": "Щебень",
                            "Kod": "CNT00009954",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-8-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-8-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Щебень",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "b92d1024-4021-11e5-86c2-40f2e9cd30ba",
                            "Name": "Ступени лестничные сборные железобетонные ЛС-15-Б",
                            "Kod": "HL200007291",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1502KAM-30-13-780",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1502KAM-30-13-780",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Ступени лестничные сборные железобетонные ЛС-15-Б",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f46-ebc7-11e3-b049-002590952079",
                            "Name": "Керамзитобетон",
                            "Kod": "CNT00009732",
                            "CheckPlanPA": false,
                            "EstimatedCode": "30-1-9999",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-1-9999",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Керамзитобетон",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "3b7ad83c-17c1-11df-9177-505054503030",
                            "Name": "Минплита",
                            "Kod": "00000003027",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Минплита",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "64ff0164-ebc6-11e3-b049-002590952079",
                            "Name": "Пленка оберточная гидроизоляционная ПДБ, толщина 0,55 мм, (ТУ 21-2749-76)",
                            "Kod": "CNT00000642",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-504",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-504",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пленка оберточная гидроизоляционная ПДБ, толщина 0,55 мм, (ТУ 21-2749-76)",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "20b718f8-65a3-11e5-af40-40f2e9cd30ba",
                            "Name": "Гидроизоляционная ветрозащитная паропроницаемая мембрана Изоспан А",
                            "Kod": "HL200007455",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-9326",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-9326",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Гидроизоляционная ветрозащитная паропроницаемая мембрана Изоспан А",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "4ed0951f-ebc7-11e3-b049-002590952079",
                            "Name": "Песок керамзитовый, М-800",
                            "Kod": "CNT00010053",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-8-102",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-8-102",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Песок керамзитовый, М-800",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "ebe88052-ee64-11e4-86c2-40f2e9cd30ba",
                            "Name": "Кирпич керамический полнотелый полуторный (ГОСТ 530-95*), 250х120х88мм, М-125",
                            "Kod": "HL200005540",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-6-9990",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "6e57f8cf-825e-11e3-8b9f-002590952079",
                            "Name": "1000 шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-6-9990",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Кирпич керамический полнотелый полуторный (ГОСТ 530-95*), 250х120х88мм, М-125",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e91c-ebc6-11e3-b049-002590952079",
                            "Name": "Маты теплоизоляционные URSA, марка М 11-50",
                            "Kod": "CNT00002128",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-156",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fb0-17b7-11df-9177-505054503030",
                            "Name": "Рулон"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-156",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Маты теплоизоляционные URSA, марка М 11-50",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "ca51c636-57dc-11e7-8a6a-40f2e9cd30ba",
                            "Name": "Счетчик горячей воды крыльчатый с радиомодулем, класса точности В Unimag Cyble d=15мм, в комплекте с фильтром",
                            "Kod": "HL200021462",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1605NOV-11-2-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "693cc6a2-9fe2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "комплект"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1605NOV-11-2-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Счетчик горячей воды крыльчатый с радиомодулем, класса точности В Unimag Cyble d=15мм, в комплекте с фильтром",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "6d8e4141-fa8e-11e2-80cd-1cc1de028fc2",
                            "Name": "дресва",
                            "Kod": "30-8-108   ",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b8957f3b-ad0a-11e4-bc77-e4115bceaa2c",
                            "Name": "куб"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "дресва",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "3c017b9c-8434-11e4-86f5-9c8e994b2add",
                            "Name": "Анимометр",
                            "Kod": "INC00004790",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Анимометр",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "04541293-a819-11e7-8630-40f2e9cd30ba",
                            "Name": "Гидрофобизатор Гидротекс-Ф",
                            "Kod": "HL200024133",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-3176",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947f13-17b7-11df-9177-505054503030",
                            "Name": "л"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-3176",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Гидрофобизатор Гидротекс-Ф",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "0388d9e7-ebc7-11e3-b049-002590952079",
                            "Name": "Зонты круглые из листовой стали марки 3К200 для вентиляционных шахт, d=200 мм",
                            "Kod": "CNT00006925",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-21",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-21",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К200 для вентиляционных шахт, d=200 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "7e748b50-ebc6-11e3-b049-002590952079",
                            "Name": "Рубероид подкладочный с пылевидной посыпкой РППс-250 ГОСТ 10923 93",
                            "Kod": "CNT00001609",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2259",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2259",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рубероид подкладочный с пылевидной посыпкой РППс-250 ГОСТ 10923 93",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "0388d9e8-ebc7-11e3-b049-002590952079",
                            "Name": "Зонты круглые из листовой стали марки 3К250 для вентиляционных шахт, d=250 мм",
                            "Kod": "CNT00006926",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-22",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-22",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К250 для вентиляционных шахт, d=250 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "9e679b02-ebc6-11e3-b049-002590952079",
                            "Name": "Сетка арматурная",
                            "Kod": "CNT00002978",
                            "CheckPlanPA": true,
                            "EstimatedCode": "4-1-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "4-1-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Сетка арматурная",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e897-ebc6-11e3-b049-002590952079",
                            "Name": "Утеплитель",
                            "Kod": "CNT00001995",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-23",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-23",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Утеплитель",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f45-ebc7-11e3-b049-002590952079",
                            "Name": "Пескобетон МБ100",
                            "Kod": "CNT00009731",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-1-9949",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-1-9949",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пескобетон МБ100",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "782dc5b5-ebc6-11e3-b049-002590952079",
                            "Name": "Пена монтажная для герметизации стыков, баллончик емкостью 750 мл",
                            "Kod": "CNT00001300",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-1163",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-1163",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пена монтажная для герметизации стыков, баллончик емкостью 750 мл",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "22625229-4468-11e4-ba7f-40f2e9cd30ba",
                            "Name": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭКП 2,6",
                            "Kod": "HL200000006",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-9693",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-9693",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭКП 2,6",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-03-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f48-ebc7-11e3-b049-002590952079",
                            "Name": "Раствор",
                            "Kod": "CNT00009734",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-2-1",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-2-1",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Раствор",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "f240edb8-be72-11e5-8a95-40f2e9cd30ba",
                            "Name": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭПП 4,0",
                            "Kod": "HL200008234",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-8848",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "4ed4450c-d37f-11e4-818e-68b599b09899",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-8848",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭПП 4,0",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c09024-ebc7-11e3-b049-002590952079",
                            "Name": "Щебень",
                            "Kod": "CNT00009954",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-8-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-8-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Щебень",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "b92d1024-4021-11e5-86c2-40f2e9cd30ba",
                            "Name": "Ступени лестничные сборные железобетонные ЛС-15-Б",
                            "Kod": "HL200007291",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1502KAM-30-13-780",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1502KAM-30-13-780",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Ступени лестничные сборные железобетонные ЛС-15-Б",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e8de-ebc6-11e3-b049-002590952079",
                            "Name": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 100",
                            "Kod": "CNT00002066",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-94",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-94",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 100",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f46-ebc7-11e3-b049-002590952079",
                            "Name": "Керамзитобетон",
                            "Kod": "CNT00009732",
                            "CheckPlanPA": false,
                            "EstimatedCode": "30-1-9999",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-1-9999",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Керамзитобетон",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "20b718f8-65a3-11e5-af40-40f2e9cd30ba",
                            "Name": "Гидроизоляционная ветрозащитная паропроницаемая мембрана Изоспан А",
                            "Kod": "HL200007455",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-9326",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-9326",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Гидроизоляционная ветрозащитная паропроницаемая мембрана Изоспан А",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e8dd-ebc6-11e3-b049-002590952079",
                            "Name": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 75",
                            "Kod": "CNT00002065",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-93",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-93",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 75",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "64ff0164-ebc6-11e3-b049-002590952079",
                            "Name": "Пленка оберточная гидроизоляционная ПДБ, толщина 0,55 мм, (ТУ 21-2749-76)",
                            "Kod": "CNT00000642",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-504",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-504",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пленка оберточная гидроизоляционная ПДБ, толщина 0,55 мм, (ТУ 21-2749-76)",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "0388d9e7-ebc7-11e3-b049-002590952079",
                            "Name": "Зонты круглые из листовой стали марки 3К200 для вентиляционных шахт, d=200 мм",
                            "Kod": "CNT00006925",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-21",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-21",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К200 для вентиляционных шахт, d=200 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "83f06c85-447e-11e4-ba7f-40f2e9cd30ba",
                            "Name": "Счетчик холодной воды с радиомодулем, класса точности С Flodis d=15мм",
                            "Kod": "HL200000134",
                            "CheckPlanPA": false,
                            "EstimatedCode": "11-2-9937",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "11-2-9937",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Счетчик холодной воды с радиомодулем, класса точности С Flodis d=15мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "782dc62a-ebc6-11e3-b049-002590952079",
                            "Name": "Геомембрана (СТО ТОО 50404263-032008), гм KGS 1,0",
                            "Kod": "CNT00001417",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-1392",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-1392",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Геомембрана (СТО ТОО 50404263-032008), гм KGS 1,0",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "3b7ad83c-17c1-11df-9177-505054503030",
                            "Name": "Минплита",
                            "Kod": "00000003027",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Минплита",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "ca51c636-57dc-11e7-8a6a-40f2e9cd30ba",
                            "Name": "Счетчик горячей воды крыльчатый с радиомодулем, класса точности В Unimag Cyble d=15мм, в комплекте с фильтром",
                            "Kod": "HL200021462",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1605NOV-11-2-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "693cc6a2-9fe2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "комплект"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1605NOV-11-2-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Счетчик горячей воды крыльчатый с радиомодулем, класса точности В Unimag Cyble d=15мм, в комплекте с фильтром",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e91c-ebc6-11e3-b049-002590952079",
                            "Name": "Маты теплоизоляционные URSA, марка М 11-50",
                            "Kod": "CNT00002128",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-156",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fb0-17b7-11df-9177-505054503030",
                            "Name": "Рулон"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-156",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Маты теплоизоляционные URSA, марка М 11-50",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "6d8e4141-fa8e-11e2-80cd-1cc1de028fc2",
                            "Name": "дресва",
                            "Kod": "30-8-108   ",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b8957f3b-ad0a-11e4-bc77-e4115bceaa2c",
                            "Name": "куб"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "дресва",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "7e748b50-ebc6-11e3-b049-002590952079",
                            "Name": "Рубероид подкладочный с пылевидной посыпкой РППс-250 ГОСТ 10923 93",
                            "Kod": "CNT00001609",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2259",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2259",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рубероид подкладочный с пылевидной посыпкой РППс-250 ГОСТ 10923 93",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "3c017b9c-8434-11e4-86f5-9c8e994b2add",
                            "Name": "Анимометр",
                            "Kod": "INC00004790",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Анимометр",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "71ab7e08-ebc6-11e3-b049-002590952079",
                            "Name": "Топливо моторное, марка ДТ",
                            "Kod": "CNT00001120",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-983",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "cde7cd8d-ebc3-11e3-b049-002590952079",
                            "Name": "т"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-983",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Топливо моторное, марка ДТ",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "04541293-a819-11e7-8630-40f2e9cd30ba",
                            "Name": "Гидрофобизатор Гидротекс-Ф",
                            "Kod": "HL200024133",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-3176",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947f13-17b7-11df-9177-505054503030",
                            "Name": "л"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-3176",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Гидрофобизатор Гидротекс-Ф",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "9e679b02-ebc6-11e3-b049-002590952079",
                            "Name": "Сетка арматурная",
                            "Kod": "CNT00002978",
                            "CheckPlanPA": true,
                            "EstimatedCode": "4-1-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "4-1-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Сетка арматурная",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "4ed0951f-ebc7-11e3-b049-002590952079",
                            "Name": "Песок керамзитовый, М-800",
                            "Kod": "CNT00010053",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-8-102",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-8-102",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Песок керамзитовый, М-800",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "0388d9e8-ebc7-11e3-b049-002590952079",
                            "Name": "Зонты круглые из листовой стали марки 3К250 для вентиляционных шахт, d=250 мм",
                            "Kod": "CNT00006926",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-22",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-22",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К250 для вентиляционных шахт, d=250 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "5b13bc23-ceb4-11e7-a678-40f2e9cd30ba",
                            "Name": "Зонты круглые из листовой стали марки 3К-125 для вентиляционных шахт, d=125мм",
                            "Kod": "HL200025173",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-9030",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "e0e4a60f-eb48-11e1-8e1f-9c8e994b2add",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-9030",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К-125 для вентиляционных шахт, d=125мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "f2de8c78-57db-11e7-8a6a-40f2e9cd30ba",
                            "Name": "Праймер битумный Bitumast TM",
                            "Kod": "HL200021365",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2810",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "7eb1013e-1464-11df-85d4-505054503030",
                            "Name": "кг"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2810",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Праймер битумный Bitumast TM",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e897-ebc6-11e3-b049-002590952079",
                            "Name": "Утеплитель",
                            "Kod": "CNT00001995",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-23",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-23",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Утеплитель",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "22625229-4468-11e4-ba7f-40f2e9cd30ba",
                            "Name": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭКП 2,6",
                            "Kod": "HL200000006",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-9693",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-9693",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭКП 2,6",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "782dc5b5-ebc6-11e3-b049-002590952079",
                            "Name": "Пена монтажная для герметизации стыков, баллончик емкостью 750 мл",
                            "Kod": "CNT00001300",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-1163",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-1163",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пена монтажная для герметизации стыков, баллончик емкостью 750 мл",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f48-ebc7-11e3-b049-002590952079",
                            "Name": "Раствор",
                            "Kod": "CNT00009734",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-2-1",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-2-1",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Раствор",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "ebe88052-ee64-11e4-86c2-40f2e9cd30ba",
                            "Name": "Кирпич керамический полнотелый полуторный (ГОСТ 530-95*), 250х120х88мм, М-125",
                            "Kod": "HL200005540",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-6-9990",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "6e57f8cf-825e-11e3-8b9f-002590952079",
                            "Name": "1000 шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-6-9990",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Кирпич керамический полнотелый полуторный (ГОСТ 530-95*), 250х120х88мм, М-125",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f45-ebc7-11e3-b049-002590952079",
                            "Name": "Пескобетон МБ100",
                            "Kod": "CNT00009731",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-1-9949",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-1-9949",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пескобетон МБ100",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-04-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "7e748b47-ebc6-11e3-b049-002590952079",
                            "Name": "Листы гипсокартонные ГКЛВ, ГОСТ 6266-97, толщина 9,5 мм",
                            "Kod": "CNT00001600",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2191",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2191",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Листы гипсокартонные ГКЛВ, ГОСТ 6266-97, толщина 9,5 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "64ff0164-ebc6-11e3-b049-002590952079",
                            "Name": "Пленка оберточная гидроизоляционная ПДБ, толщина 0,55 мм, (ТУ 21-2749-76)",
                            "Kod": "CNT00000642",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-504",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-504",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пленка оберточная гидроизоляционная ПДБ, толщина 0,55 мм, (ТУ 21-2749-76)",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e8de-ebc6-11e3-b049-002590952079",
                            "Name": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 100",
                            "Kod": "CNT00002066",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-94",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-94",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 100",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "f240edb8-be72-11e5-8a95-40f2e9cd30ba",
                            "Name": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭПП 4,0",
                            "Kod": "HL200008234",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-8848",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "4ed4450c-d37f-11e4-818e-68b599b09899",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-8848",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рулонные наплавляемые кровельные и гидроизоляционные материалы ТехноНиколь Техноэласт ЭПП 4,0",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "0388d9e8-ebc7-11e3-b049-002590952079",
                            "Name": "Зонты круглые из листовой стали марки 3К250 для вентиляционных шахт, d=250 мм",
                            "Kod": "CNT00006926",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-22",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-22",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К250 для вентиляционных шахт, d=250 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e8dd-ebc6-11e3-b049-002590952079",
                            "Name": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 75",
                            "Kod": "CNT00002065",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-93",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-93",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Плиты теплоизоляционные из минеральной ваты на битумном связующем, марка 75",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "4ed0951f-ebc7-11e3-b049-002590952079",
                            "Name": "Песок керамзитовый, М-800",
                            "Kod": "CNT00010053",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-8-102",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-8-102",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Песок керамзитовый, М-800",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "04541293-a819-11e7-8630-40f2e9cd30ba",
                            "Name": "Гидрофобизатор Гидротекс-Ф",
                            "Kod": "HL200024133",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-3176",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947f13-17b7-11df-9177-505054503030",
                            "Name": "л"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-3176",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Гидрофобизатор Гидротекс-Ф",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "9e679b02-ebc6-11e3-b049-002590952079",
                            "Name": "Сетка арматурная",
                            "Kod": "CNT00002978",
                            "CheckPlanPA": true,
                            "EstimatedCode": "4-1-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "4-1-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Сетка арматурная",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "782dc62a-ebc6-11e3-b049-002590952079",
                            "Name": "Геомембрана (СТО ТОО 50404263-032008), гм KGS 1,0",
                            "Kod": "CNT00001417",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-1392",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-1392",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Геомембрана (СТО ТОО 50404263-032008), гм KGS 1,0",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "ca51c636-57dc-11e7-8a6a-40f2e9cd30ba",
                            "Name": "Счетчик горячей воды крыльчатый с радиомодулем, класса точности В Unimag Cyble d=15мм, в комплекте с фильтром",
                            "Kod": "HL200021462",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1605NOV-11-2-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "693cc6a2-9fe2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "комплект"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1605NOV-11-2-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Счетчик горячей воды крыльчатый с радиомодулем, класса точности В Unimag Cyble d=15мм, в комплекте с фильтром",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "3b7ad83c-17c1-11df-9177-505054503030",
                            "Name": "Минплита",
                            "Kod": "00000003027",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Минплита",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "5b13bc23-ceb4-11e7-a678-40f2e9cd30ba",
                            "Name": "Зонты круглые из листовой стали марки 3К-125 для вентиляционных шахт, d=125мм",
                            "Kod": "HL200025173",
                            "CheckPlanPA": false,
                            "EstimatedCode": "13-12-9030",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "e0e4a60f-eb48-11e1-8e1f-9c8e994b2add",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "13-12-9030",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Зонты круглые из листовой стали марки 3К-125 для вентиляционных шахт, d=125мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "8b90e91c-ebc6-11e3-b049-002590952079",
                            "Name": "Маты теплоизоляционные URSA, марка М 11-50",
                            "Kod": "CNT00002128",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-3-156",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fb0-17b7-11df-9177-505054503030",
                            "Name": "Рулон"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-3-156",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Маты теплоизоляционные URSA, марка М 11-50",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "7e748b50-ebc6-11e3-b049-002590952079",
                            "Name": "Рубероид подкладочный с пылевидной посыпкой РППс-250 ГОСТ 10923 93",
                            "Kod": "CNT00001609",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2259",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2259",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Рубероид подкладочный с пылевидной посыпкой РППс-250 ГОСТ 10923 93",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "ebe88052-ee64-11e4-86c2-40f2e9cd30ba",
                            "Name": "Кирпич керамический полнотелый полуторный (ГОСТ 530-95*), 250х120х88мм, М-125",
                            "Kod": "HL200005540",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-6-9990",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "6e57f8cf-825e-11e3-8b9f-002590952079",
                            "Name": "1000 шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-6-9990",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Кирпич керамический полнотелый полуторный (ГОСТ 530-95*), 250х120х88мм, М-125",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "6d8e4141-fa8e-11e2-80cd-1cc1de028fc2",
                            "Name": "дресва",
                            "Kod": "30-8-108   ",
                            "CheckPlanPA": false,
                            "EstimatedCode": "",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b8957f3b-ad0a-11e4-bc77-e4115bceaa2c",
                            "Name": "куб"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "дресва",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c08f45-ebc7-11e3-b049-002590952079",
                            "Name": "Пескобетон МБ100",
                            "Kod": "CNT00009731",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-1-9949",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-1-9949",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пескобетон МБ100",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "782dc5b5-ebc6-11e3-b049-002590952079",
                            "Name": "Пена монтажная для герметизации стыков, баллончик емкостью 750 мл",
                            "Kod": "CNT00001300",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-1163",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-1163",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Пена монтажная для герметизации стыков, баллончик емкостью 750 мл",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "b92d1024-4021-11e5-86c2-40f2e9cd30ba",
                            "Name": "Ступени лестничные сборные железобетонные ЛС-15-Б",
                            "Kod": "HL200007291",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1502KAM-30-13-780",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1502KAM-30-13-780",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Ступени лестничные сборные железобетонные ЛС-15-Б",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "7e748b47-ebc6-11e3-b049-002590952079",
                            "Name": "Листы гипсокартонные ГКЛВ, ГОСТ 6266-97, толщина 9,5 мм",
                            "Kod": "CNT00001600",
                            "CheckPlanPA": false,
                            "EstimatedCode": "1-1-2191",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "b9947fd4-17b7-11df-9177-505054503030",
                            "Name": "м2"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "1-1-2191",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Листы гипсокартонные ГКЛВ, ГОСТ 6266-97, толщина 9,5 мм",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "48c09024-ebc7-11e3-b049-002590952079",
                            "Name": "Щебень",
                            "Kod": "CNT00009954",
                            "CheckPlanPA": true,
                            "EstimatedCode": "30-8-2",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "bff3487a-17b7-11df-9177-505054503030",
                            "Name": "м3"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "30-8-2",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Щебень",
                        "PaymentMethod": ""
                    },
                    {
                        "PeriodValidity": "2018-05-01T00:00:00Z",
                        "TypeOfWork": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "TechnicalSpecification": "Нет",
                        "Nomenclature": {
                            "Guid": "83f06c85-447e-11e4-ba7f-40f2e9cd30ba",
                            "Name": "Счетчик холодной воды с радиомодулем, класса точности С Flodis d=15мм",
                            "Kod": "HL200000134",
                            "CheckPlanPA": false,
                            "EstimatedCode": "11-2-9937",
                            "DefaultUnit": {
                                "Guid": "00000000-0000-0000-0000-000000000000",
                                "Name": ""
                            }
                        },
                        "Unit": {
                            "Guid": "f62405d7-9fc2-11e5-a1eb-40f2e9cbd84d",
                            "Name": "шт"
                        },
                        "Count": 0,
                        "CountTotal": 0,
                        "Price": 0,
                        "Sum": 0,
                        "SumTotal": 0,
                        "SumWithVAT": 0,
                        "PurchaseMethod": "",
                        "DeliveryConditions": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "DeliverySchedule": 0,
                        "DeliveryScheduleDecade1": 0,
                        "DeliveryScheduleDecade2": 0,
                        "DeliveryScheduleDecade3": 0,
                        "PaymentConditions": "",
                        "Subproject": {
                            "Guid": "00000000-0000-0000-0000-000000000000",
                            "Name": ""
                        },
                        "CodeTRU": "11-2-9937",
                        "Note": "",
                        "MethodKralich": "",
                        "FullName": "Счетчик холодной воды с радиомодулем, класса точности С Flodis d=15мм",
                        "PaymentMethod": ""
                    }
                ]
            }
        ]
    };
    



module.exports = mockData;

