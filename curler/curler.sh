curl -X POST \
  https://bi_service:9am87veaji5ae7twvnss@pre-opera.bi.group/edge/start-fixdefect-process \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: ddd06db0-0b1d-4925-a5cd-2267a8b3d096' \
  -H 'cache-control: no-cache' \
  -d '{
    "id": "OOO00130372",
    "index": 1,
    "type_id": "3eacefe8-8b52-11e4-bd2b-0025906b4dd5",
    "type_name": "Вентиляционная шахта - не вытягивает воздух",
    "comment": "дует с вент. шахты ",
    "apt_owner_id": null,
    "apt_owner_name": "Узукеева Снайда Бообековна",
    "apt_owner_phone": "87777340100",
    "apt_number": "106",
    "date": "2019-01-19T10:23:57.2",
    "deadline": null,
    "deadline_time": "",
    "cost": "0.00",
    "contact_person_name": "Узукеева Снайда Бообековна",
    "contact_person_phone": "87777340100",
    "photo": "",
    "assignee_id": "00000000-0000-0000-0000-000000000000",
    "assignee_name": "",
    "real_estate_id": "8bc78b29-f23b-11e5-b34f-b4b52f5405e7",
    "real_estate_block_id": "1118ea3d-8b1c-11e3-a326-0025906b4dd5",
    "service_property_name": "Gagarin park ЖК (3 очередь) Блок B"
  
}'
