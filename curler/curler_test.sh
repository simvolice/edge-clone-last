curl -X POST \
  https://bi_service:9am87veaji5ae7twvnss@pre-opera.bi.group/edge/start-fixdefect-process \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: ddd06db0-0b1d-4925-a5cd-2267a8b3d096' \
  -H 'cache-control: no-cache' \
  -d '{
  "id": "123456789",
  "index": 8,
  "type_id": "установка/замена электросчетчика",
  "type_name": "установка/замена электросчетчdsaика",
  "comment": "The outside door #3 lock needs to be fixed",
  "apt_owner_id": "Ivanov AB",
  "apt_owner_name": "Ivanov AB",
  "apt_owner_phone": "8-778-831-27-88",
  "date": "2018-11-14",
  "deadline": "2018-11-27",
  "deadline_time": "0",
  "cost": "123000",
  "contact_person_name": "Alex Adams",
  "contact_person_phone": "77015339388",
  "real_estate_id": "8bc78b29-f23b-11e5-b34f-b4b52f5405e7",
  "real_estate_block_id": "bbba12c7-d436-11e3-882b-0025906b4dd5",
  "assignee_id": "demo",
  "assignee_name": "Demo Demo",
  "service_property_name": "Достар 3-3 Офисы",
    "apt_number": "218",
  "photo": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg=="
}'
