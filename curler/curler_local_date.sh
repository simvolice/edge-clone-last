curl -X POST \
  http://localhost:3001/start-fixdefect-process \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: ddd06db0-0b1d-4925-a5cd-2267a8b3d096' \
  -H 'cache-control: no-cache' \
  -d '{  
   "id":"OOO00129899",
   "type_id":"3eacefd2-8b52-11e4-bd2b-0025906b4dd5",
   "type_name":"Пол - холодный",
   "comment":"пол холодный, запах плесени, образуется конденсат на ламинате ",
   "apt_owner_id":"73ad1a18-4135-475b-87d3-41462b4e45b2",
   "apt_owner_name":"Жумабаева Мадина Хасеновна",
   "apt_owner_phone":"87017472381",
   "apt_number":"160",
   "date":"2019-01-16T12:47:52.827",
   "deadline":null,
   "deadline_time":"",
   "cost":"0,00",
   "contact_person_name":"Алиев Женис (новый владелец)",
   "contact_person_phone":"",
   "photo":"",
   "assignee_id":"bb4620e8-b0d1-11e8-80d7-00155da7893d",
   "assignee_name":"Калкузов Е.Н.",
   "real_estate_id":null,
   "real_estate_block_id":"69037608-876a-11e4-bd2b-0025906b4dd5",
   "service_property_name":"Камал ЖК 5 оч."
}'